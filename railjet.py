from time import sleep
import time

from gi.repository import GObject
from gi.repository import Notify

import requests

STATUS_URL = "https://railnet.oebb.at/assets/modules/fis/combined.json?_time=%s"

lastSpeed = -1.0
Notify.init("RailJet Speed widget")
notification = Notify.Notification
while True:
    status = requests.get(STATUS_URL % (round(time.time() * 1000))).json()

    speed = status["operationalMessagesInfo"]["speed"]

    if speed != lastSpeed:
        print("Speed: %s" % speed)
        notification = Notify.Notification.new("RailJet speed change", "New speed: %s km/h" % speed)
        notification.show()
        lastSpeed = speed
    sleep(10)
    notification.close()
