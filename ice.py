from time import sleep

from gi.repository import GObject
from gi.repository import Notify

import requests

STATUS_URL = "https://iceportal.de/api1/rs/status"

lastSpeed = -1.0
Notify.init("ICE Speed widget")
notification = Notify.Notification
while True:
    status = requests.get(STATUS_URL).json()

    speed = status["speed"]

    if speed != lastSpeed:
        print("Speed: %s" % speed)
        notification = Notify.Notification.new("ICE speed change", "New speed: %s km/h" % speed)
        notification.show()
        lastSpeed = speed
    sleep(2)
    notification.close()
